all: 1-crc32-simple 2-crc32-with-table 3-crc32-precomp-table 4-crc32-constexpr-magic



1-crc32-simple:	1-crc32-simple.cpp
	g++ -std=c++17 -Wall -o $@ $<

2-crc32-with-table:	2-crc32-with-table.cpp
	g++ -std=c++17 -Wall -o $@ $<

3-crc32-precomp-table:	3-crc32-precomp-table.cpp
	g++ -std=c++17 -Wall -o $@ $<

4-crc32-constexpr-magic:	4-crc32-constexpr-magic.cpp make_compile_time_array.h
	g++ -std=c++17 -Wall -o $@ $<

