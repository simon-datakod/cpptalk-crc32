# CRC32 Implementation in C++

There are four different versions:

### 1-crc32-simple.cpp
Straight forward, but slow implementation

### 2-crc32-with-table.cpp
Slightly better - precomputes a 1K table on initialization so CRC32 calculations is a lot faster 

### 3-crc32-precomp-table.cpp
Same as version 2, but the table is now just static data - so no need to calculate it on startup, and we get rid of the code to calculate it as well.

### 4-crc32-constexpr-magic.cpp
Essentially the same as version 3, but much more flexible, where we can instantiate CRC32 objects with arbitrary polynomials without having to manually recreate the table.


# Usage
(Only tested in Linux and with gcc 7.2.0)

Just run `make all' and/or ./test_all.sh

# License
The seq/gen\_seq snippet in make_compile_time_array.h is nicked off of Stack Overflow (so their rules apply). All the other stuff under MIT license.
