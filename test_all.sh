#!/bin/bash

TEST_FILE=/etc/passwd

make all
/usr/bin/crc32 $TEST_FILE
./1-crc32-simple $TEST_FILE
./2-crc32-with-table $TEST_FILE
./3-crc32-precomp-table $TEST_FILE
./4-crc32-constexpr-magic $TEST_FILE

