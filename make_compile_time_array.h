#pragma once
#include <array>

struct detail
{
    /* Uses the "indices trick" for generating a sequence 0, 1, 2, ..., N-1 to map the function F over.
     *
     *  http://stackoverflow.com/questions/13313980/populate-an-array-using-constexpr-at-compile-time
     *  http://loungecpp.net/cpp/indices-trick/
    */

    template<int... Is>
    struct seq { };

    template<int N, int... Is>
    struct gen_seq : gen_seq<N-1, N-1, Is...>{ };

    template<int... Is>
    struct gen_seq<0, Is...> : seq<Is...>{ };

    template <typename T, std::size_t N, T (*F)(int), int... Is>
    static constexpr std::array<T, N> make_array(seq<Is...>) {
        return { F(Is)... };
    }
};


/*
 *  Will construct an std::array<T,N> in compile time, populating the array by mapping the indexes
 *  (0, 1, 2, ... N-1) to the supplied function F.
 *
 *  Template parameters:
 *      T     Type of the elements of the array
 *      N     Number of elements in the array
 *      F     Function that takes an index `int i' as parameter and returns a value of type T
 *
 *  Example:
 *    
 *      static constexpr char get_char(int i) { return 'A' + i; }
 *      static constexpr auto array = make_compile_time_array<char, 5, get_char>();
 *
 *  ... this is equivalent of:
 *
 *      static constexpr std::array<char,5> array = { 'A', 'B', 'C', 'D', 'E' };
 *
 */

template <typename T, std::size_t N, T (*F)(int)>
static constexpr std::array<T, N> make_compile_time_array() {
    return detail::make_array<T, N, F>(detail::gen_seq<N>{});
}
