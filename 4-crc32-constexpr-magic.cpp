#include <iostream>
#include <iomanip>
#include <array>
#include <fstream>
#include <cerrno>
#include <cstring>
#include <cstdint>
#include "make_compile_time_array.h"


template <uint32_t Polynomial = 0xedb88320>
class CRC32 final {

    public:
        void add(const uint8_t * data, std::size_t size) {
            for (auto ptr = data; ptr < data + size; ptr++) {
                crc_ = (crc_ >> 8) ^ precomp_table_[ (crc_ ^ (*ptr & 0xff)) & 0xff ];
            }
        }

        uint32_t result() const { return crc_ ^ ~0; }

    private:
        struct detail {
            static constexpr inline uint32_t calc_crc_value(int i)
            {
                uint32_t crc = i;
                for (int j = 0; j < 8; j++) {
                    if (crc & 1) {
                        crc = ( crc >> 1 ) ^ Polynomial;
                    } else {
                        crc = crc >> 1;
                    }
                }
                return crc;
            }
        };

    private:
        uint32_t    crc_ = ~0;

        static const constexpr auto precomp_table_ = make_compile_time_array<uint32_t, 256, detail::calc_crc_value>();
};


int main(int argc, char * argv[]) {
    if (argc != 2) {
        std::cerr << "usage: " << argv[0] << " <file>\n";
        return 1;
    }

    const auto path = std::string { argv[1] };

    if (std::ifstream in { path, std::ios::binary }; in.fail())  {
        std::cerr << argv[1] << ": " << strerror(errno) << "\n";
        return 1;
    } else {
        uint8_t buf[100];
        auto crc32 = CRC32 { };
        while (!in.eof()) {
            in.read(reinterpret_cast<char *>(buf), sizeof(buf));
            crc32.add(buf, in.gcount());
        }
        std::cout << path << ": " << std::setfill('0') << std::setw(8) << std::hex << crc32.result() << "\n";
    }

    return 0;
}
