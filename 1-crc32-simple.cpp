#include <iostream>
#include <iomanip>
#include <array>
#include <fstream>
#include <cerrno>
#include <cstring>
#include <cstdint>


class CRC32 final {
    public:
        CRC32(uint32_t polynomial = 0xedb88320) : polynomial_ { polynomial } { }

        void add(const uint8_t * data, std::size_t size) {
            for (auto ptr = data; ptr < data + size; ptr++) {
                auto v = calc_crc_value((crc_ ^ (*ptr & 0xff)) & 0xff);
                crc_ = (crc_ >> 8) ^ v;
            }
        }

        uint32_t result() const { return crc_ ^ ~0; }

    private:
        inline uint32_t calc_crc_value(unsigned i) const {
            uint32_t crc = i;
            for (int j = 0; j < 8; j++) {
                if (crc & 1) {
                    crc = ( crc >> 1 ) ^ polynomial_;
                } else {
                    crc = crc >> 1;
                }
            }
            return crc;
        }

    private:
        const uint32_t  polynomial_;
        uint32_t        crc_ = ~0;
};


int main(int argc, char * argv[]) {
    if (argc != 2) {
        std::cerr << "usage: " << argv[0] << " <file>\n";
        return 1;
    }

    const auto path = std::string { argv[1] };

    if (std::ifstream in { path, std::ios::binary }; in.fail())  {
        std::cerr << argv[1] << ": " << strerror(errno) << "\n";
        return 1;
    } else {
        uint8_t buf[100];
        auto crc32 = CRC32 { };
        while (!in.eof()) {
            in.read(reinterpret_cast<char *>(buf), sizeof(buf));
            crc32.add(buf, in.gcount());
        }
        std::cout << path << ": " << std::setfill('0') << std::setw(8) << std::hex << crc32.result() << "\n";
    }

    return 0;
}
